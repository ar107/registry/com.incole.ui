﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

public class LayoutMethods : MonoBehaviour
{
    [InlineButton("GetRect", "Init")]
    public RectTransform Target;

    private void GetRect()
    {
        Target = GetComponent<RectTransform>();
    }

    public void RecalcLayout()
    {
        LayoutRebuilder.ForceRebuildLayoutImmediate(Target);
    }
}
﻿using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using System.Threading;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TimedEvent : MonoBehaviour
{
    public bool RunOnStart = false;
    public UnityEvent Action;

    [ShowIf("RunOnStart")]
    public float Timeout;

    private CancellationTokenSource _cancellationTokenSource;

    public void Start()
    {
        if (!RunOnStart)
            return;

        Run(Timeout);
    }

    public async void Run(float timeout)
    {
        if (_cancellationTokenSource != null)
            return;

        _cancellationTokenSource = new CancellationTokenSource();

        var token = _cancellationTokenSource.Token;

        await UniTask.Delay(Mathf.FloorToInt(1000 * timeout), cancellationToken: token).SuppressCancellationThrow();

        if (token.IsCancellationRequested)
            return;

        Action.Invoke();

        _cancellationTokenSource = null;
    }

    public void Cancel()
    {
        if (_cancellationTokenSource == null)
            return;
        
        _cancellationTokenSource.Cancel();
        _cancellationTokenSource = null;
    }
}
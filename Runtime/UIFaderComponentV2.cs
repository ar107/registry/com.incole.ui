﻿using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

public class UIFaderComponentV2 : MonoBehaviour
{
    [InlineButton("GetNeighbourCanvas", "Init")]
    public CanvasGroup UiElement;
    public float Speed = 1f;
    public bool UnscaledTime = false;

    [FoldoutGroup("Additional", expanded: false)]
    public bool ControlsRaycast = true;
    [FoldoutGroup("Additional", expanded: false)]
    public bool ControlsInteractable = true;

    [FoldoutGroup("Events", expanded: false)]
    public UnityEvent OnStartShow;
    [FoldoutGroup("Events", expanded: false)]
    public UnityEvent OnShown;
    [FoldoutGroup("Events", expanded: false)]
    public UnityEvent OnStartHide;
    [FoldoutGroup("Events", expanded: false)]
    public UnityEvent OnHidden;

    

    private CancellationTokenSource _cancellationTokenSource;

    public float TargetAlpha { get; private set; }

    private void GetNeighbourCanvas()
    {
        UiElement = GetComponent<CanvasGroup>();
    }

    public void FadeIn()
    {
        FadeIn(Speed);
    }

    public void FadeOut()
    {
        FadeOut(Speed);
    }

    public void Toggle()
    {
        if (TargetAlpha == 1)
            FadeOut();
        else
            FadeIn();
    }

    public async void Fade(float speed, float targetAlpha)
    {
        RaiseBeginEvent(1);
        Speed = speed;
        TargetAlpha = targetAlpha;

        await FadeCanvasGroupAsync();
    }

    public async void FadeIn(float speed)
    {
        RaiseBeginEvent(1);
        Speed = speed;
        TargetAlpha = 1;

        await FadeCanvasGroupAsync();
    }

    public async void FadeOut(float speed)
    {
        RaiseBeginEvent(0);
        Speed = speed;
        TargetAlpha = 0;

        await FadeCanvasGroupAsync();
    }

    public async UniTask FadeInAsync(float speed)
    {
        RaiseBeginEvent(1);
        Speed = speed;
        TargetAlpha = 1;

        await FadeCanvasGroupAsync();
    }

    public async UniTask FadeOutAsync(float speed)
    {
        RaiseBeginEvent(0);
        Speed = speed;
        TargetAlpha = 0;

        await FadeCanvasGroupAsync();
    }

    public async UniTask FadeInAsync()
    {
        await FadeInAsync(Speed);
    }

    public async UniTask FadeOutAsync()
    {
        await FadeOutAsync(Speed);
    }

    public async UniTask FadeCanvasGroupAsync()
    {
        if (_cancellationTokenSource != null)
            _cancellationTokenSource.Cancel();

        if (UiElement.alpha == TargetAlpha)
            return;

        _cancellationTokenSource = new CancellationTokenSource();

        var token = _cancellationTokenSource;

        while (UiElement.alpha != TargetAlpha)
        {
            var deltaTime = UnscaledTime ? Time.unscaledTime : Time.deltaTime;

            ApplyOpacity(deltaTime * Speed);

            if (Mathf.Abs(UiElement.alpha - TargetAlpha) <= 0.05f)
            {
                ApplyOpacity(1);

                if (TargetAlpha == 1)
                    OnShown.Invoke();
                else
                    OnHidden.Invoke();

                _cancellationTokenSource = null;
                return;
            }

            await UniTask.NextFrame();

            if (token.IsCancellationRequested)
                return;
        }

        _cancellationTokenSource = null;
    }

    private void RaiseBeginEvent(float targetAlpha)
    {
        if (targetAlpha == TargetAlpha)
            return;

        if (targetAlpha == 1)
            OnStartShow.Invoke();
        else
            OnStartHide.Invoke();
    }

    private void ApplyOpacity(float t)
    {
        UiElement.alpha = Mathf.Lerp(UiElement.alpha, TargetAlpha, t);

        if (ControlsRaycast) UiElement.blocksRaycasts = UiElement.alpha == 1;
        if (ControlsInteractable) UiElement.interactable = UiElement.alpha == 1;
    }

    [ButtonGroup("Buttons")]
    [Button("Hide")]
    public void ImmediatelyHide()
    {
        if (_cancellationTokenSource != null)
            _cancellationTokenSource.Cancel();

        if (UiElement == null)
            return;

        if (UiElement.alpha == 0)
            return;

        if (TargetAlpha != 0)
            OnStartHide.Invoke();

        TargetAlpha = 0;
        ApplyOpacity(1);

        if (Application.isPlaying)
            OnHidden.Invoke();
    }

    [ButtonGroup("Buttons")]
    [Button("Show")]
    public void ImmediatelyShow()
    {
        if (_cancellationTokenSource != null)
            _cancellationTokenSource.Cancel();

        if (UiElement == null)
            return;

        if (UiElement.alpha == 1)
            return;

        if (TargetAlpha != 1)
            OnStartShow.Invoke();

        TargetAlpha = 1;
        ApplyOpacity(1);

        if (Application.isPlaying)
            OnShown.Invoke();
    }

    public async void WaitAndShow(float delay)
    {
        if (_cancellationTokenSource != null)
            _cancellationTokenSource.Cancel();

        _cancellationTokenSource = new CancellationTokenSource();
        var token = _cancellationTokenSource.Token;

        await UniTask.Delay(Mathf.FloorToInt(delay * 1000), cancellationToken: token).SuppressCancellationThrow();
        if (token.IsCancellationRequested)
            return;

        ImmediatelyShow();
        _cancellationTokenSource = null;
    }

    public async void WaitAndHide(float delay)
    {
        if (_cancellationTokenSource != null)
            _cancellationTokenSource.Cancel();

        _cancellationTokenSource = new CancellationTokenSource();
        var token = _cancellationTokenSource.Token;

        await UniTask.Delay(Mathf.FloorToInt(delay * 1000), cancellationToken: token).SuppressCancellationThrow();
        if (token.IsCancellationRequested)
            return;

        ImmediatelyHide();
        _cancellationTokenSource = null;
    }
}